<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/







Route::group(['prefix' => 'admin'], function () {
    Route::group(['middleware' => 'admin.guest'], function () {

        Route::get('/login', [App\Http\Controllers\admin\LoginController::class, 'loginIndex'])->name('admin.login');
        Route::post('/onLogin', [App\Http\Controllers\admin\LoginController::class, 'onLogin'])->name('admin.onLogin');
    });

        Route::group(['middleware' => 'admin.auth'], function () {
        //Logout
        Route::get('/logout', [App\Http\Controllers\admin\LoginController::class, 'onLogout'])->name('admin.logout');

        // Admin Route
        Route::get('/dashboard', [App\Http\Controllers\admin\HomeController::class, 'adminHome'])->name('admin.adminHome');
        Route::get('/admin-settings', [App\Http\Controllers\admin\AdminController::class, 'AdminIndex'])->name('admin.adminPannel');
        Route::get('/getAdminData', [App\Http\Controllers\admin\AdminController::class, 'adminData'])->name('admin.getAdminData');
        Route::get('/adminRole', [App\Http\Controllers\admin\AdminController::class, 'adminRole'])->name('admin.adminRole');
        Route::post('/adminAdd', [App\Http\Controllers\admin\AdminController::class, 'adminAdd'])->name('admin.adminAdd');
        Route::post('/adminDelete', [App\Http\Controllers\admin\AdminController::class, 'adminDelete'])->name('admin.adminDelete');
        Route::post('/adminDetailEdit', [App\Http\Controllers\admin\AdminController::class, 'adminDetailEdit'])->name('admin.adminDetailEdit');
        Route::post('/adminDataUpdate', [App\Http\Controllers\admin\AdminController::class, 'adminDataUpdate'])->name('admin.adminDataUpdate');



       // User Route
       Route::get('/user-annel', [App\Http\Controllers\admin\UserController::class, 'userIndex'])->name('admin.userPannel');
       Route::get('/user-data', [App\Http\Controllers\admin\UserController::class, 'userData'])->name('admin.getUserdata');
       Route::post('/userAdd', [App\Http\Controllers\admin\UserController::class, 'userAdd'])->name('admin.userAdd');
       Route::post('/userDelete', [App\Http\Controllers\admin\UserController::class, 'userDelete'])->name('admin.userDelete');
       Route::post('/userDetailEdit', [App\Http\Controllers\admin\UserController::class, 'userDetailEdit'])->name('admin.userDetailEdit');
       Route::post('/userDataUpdate', [App\Http\Controllers\admin\UserController::class, 'userDataUpdate'])->name('admin.userDataUpdate');




        //Visitor Table
        Route::get('/visitor', [\App\Http\Controllers\admin\VisitorController::class,'VisitorIndex'])->name('admin.VisitorIndex');



        //admin panel  Configuration management with social URL
        Route::get('/configuration', [\App\Http\Controllers\admin\ConfigurationController::class,'index'])->name('admin.configuration');
        Route::post('/configuration/add', [\App\Http\Controllers\admin\ConfigurationController::class,'setEnvValue'])->name('admin.configuration.add');


        //admin panel  Setting management with social URL
        Route::get('/setting', [\App\Http\Controllers\admin\SettingController::class,'otherIndex'])->name('admin.setting');
        Route::post('/address', [\App\Http\Controllers\admin\SettingController::class,'addAddress'])->name('admin.address');
        Route::post('/phone', [\App\Http\Controllers\admin\SettingController::class,'addPhone'])->name('admin.phone');
        Route::post('/email', [\App\Http\Controllers\admin\SettingController::class,'addEmail'])->name('admin.email');
        Route::post('/title', [\App\Http\Controllers\admin\SettingController::class,'addTitle'])->name('admin.title');
        Route::post('/subTitle', [\App\Http\Controllers\admin\SettingController::class,'addsubTitle'])->name('admin.subTitle');
        Route::post('/gmap', [\App\Http\Controllers\admin\SettingController::class,'addGmap'])->name('admin.gmap');
        Route::post('/logo', [\App\Http\Controllers\admin\SettingController::class,'logoAdd'])->name('admin.logo');
        Route::post('/Banner', [\App\Http\Controllers\admin\SettingController::class,'BannerAdd'])->name('admin.Banner');
        Route::post('/siteName', [\App\Http\Controllers\admin\SettingController::class,'addSiteName'])->name('admin.siteName');


    });



});






Route::get('/', [App\Http\Controllers\client\HomeController::class, 'index'])->name('client.home');


//Login
Route::get('/login', [App\Http\Controllers\client\AuthController::class, 'showLogin'])->name('client.login');
Route::post('/onlogin', [App\Http\Controllers\client\AuthController::class, 'onlogin'])->name('client.onlogin');
Route::get('/registration', [App\Http\Controllers\client\AuthController::class, 'registration'])->name('client.registration');
Route::post('/add-user', [App\Http\Controllers\client\AuthController::class, 'addUser'])->name('client.addUser');
Route::get('/active/{token}', [App\Http\Controllers\client\AuthController::class, 'userActive'])->name('client.active');

//reset password
Route::get('/forgot', [App\Http\Controllers\client\AuthController::class, 'forgot'])->name('client.forgot');
Route::post('/forgot-password', [App\Http\Controllers\client\AuthController::class, 'forgotPassword'])->name('client.forgotPassword');
Route::get('/recover-password/{id}', [App\Http\Controllers\client\AuthController::class, 'recoverPassWord'])->name('client.recoverPassWord');
Route::post('/update-password', [App\Http\Controllers\client\AuthController::class, 'updatePassword'])->name('client.updatePassword');


// logout & Profile & Order
Route::group(['middleware' => 'auth'], function () {
    Route::get('/logout', [App\Http\Controllers\client\AuthController::class, 'logout'])->name('client.logout');
    Route::get('/profile', [App\Http\Controllers\client\AuthController::class, 'profile'])->name('client.profile');

    Route::get('/password-reset/{id}', [App\Http\Controllers\client\AuthController::class, 'passwordResetView'])->name('client.passwordReset');
    Route::post('/passwordUpdate/{id}', [App\Http\Controllers\client\AuthController::class, 'resetPassword'])->name('client.passwordUpdate');

    Route::get('/profile-edit/{id}', [App\Http\Controllers\client\AuthController::class, 'profileEdit'])->name('client.profileEdit');
    Route::post('/upadete-profile/{id}', [App\Http\Controllers\client\AuthController::class, 'upadeteProfile'])->name('client.upadeteProfile');

});

