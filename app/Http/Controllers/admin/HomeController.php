<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Order;
use App\Models\Product;
use App\Models\Brand;
use App\Models\Category;
use App\Models\User;
use App\Models\Visitor;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

    public $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('admin')->user();
            return $next($request);
        });
    }
    public function adminHome()
    {
        if (is_null($this->user) || !$this->user->can('home.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view any Dashboard !');
        }
        $data=[];
        $data['TotalVisitor']=Visitor::count();
        $data['TotalCustomer']=User::count();

        return view('admin.Home',  $data);
    }
}
