@extends('admin.Layouts.app')
@section('title','Home')
@section('content')


<div class="container">
	<div class="row">

		<div class="col-md-4 p-2">
			<div class="card">
				<div class="card-body">
					<h3 class="count-card-title">{{$TotalVisitor}}</h3>
					<h3 class="count-card-text">Total Visitor</h3>
				</div>
			</div>
		</div>


        <div class="col-md-4 p-2">
			<div class="card">
				<div class="card-body">
					<h3 class="count-card-title">{{$TotalCustomer}}</h3>
					<h3 class="count-card-text">Total Customer</h3>
				</div>
			</div>
		</div>
 

	</div>
</div>

@endsection
