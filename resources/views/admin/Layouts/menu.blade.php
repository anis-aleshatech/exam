@php
$usr = Auth::guard('admin')->user();
@endphp
<div id="main-wrapper">
    <header class="topbar">
        <nav class="navbar top-navbar navbar-expand-md navbar-light">
            <div class="navbar-collapse">
                <ul class="navbar-nav mr-auto mt-md-0">
                    <li class="nav-item "> <a class="nav-link nav-toggler  hidden-md-up  waves-effect waves-dark"
                            href="javascript:void(0)"><i class="fas  fa-bars"></i></a></li>
                    <li class="nav-item m-l-10"> <a
                            class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark"
                            href="javascript:void(0)"><i class="fas fa-bars"></i></a> </li>
                    <li class="nav-item mt-3">{{ $usr ->name }}</li>
                    <li class="nav-item mt-3"> &nbsp; ({{ ucfirst($usr ->getRoleNames()[0]) }})</li>

                </ul>
                <ul class="navbar-nav my-lg-0">
                    <li class="nav-item"><a href="{{ route('admin.logout') }}"
                            class="btn btn-sm btn-danger">Logout</a></li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="left-sidebar">
        <div class="scroll-sidebar">
            <nav class="sidebar-nav">
                <ul id="sidebarnav">
                    <li class="nav-devider mt-0" style="margin-bottom: 5px"></li>
                    @if ($usr->can('home.view'))
                        <li> <a href="{{ route('admin.adminHome') }}"><span> <i class="fas fa-home"></i> </span><span
                                    class="hide-menu">Home</span></a></li>
                    @endif
                    <li> <a href="{{ route('client.home') }}" target="_blank"><span> <i class="fas fa-globe"></i>
                            </span><span class="hide-menu">Visit Site</span></a></li>

                    @if ($usr->can('visitor.view'))
                        <li> <a href="{{ route('admin.VisitorIndex') }}"><span> <i class="fas fa-users"></i>
                                </span><span class="hide-menu">Visitor</span></a></li>
                    @endif

                    @if ($usr->can('admin.view'))
                        <li> <a href="{{ route('admin.adminPannel') }}"><span> <i class="fas fa-user"></i>
                                </span><span class="hide-menu">Admin</span></a></li>
                    @endif
                   

                    @if ($usr->can('user.view'))
                        <li> <a href="{{ route('admin.userPannel') }}"><span> <i class="fas fa-users"></i>
                                </span><span class="hide-menu">User</span></a></li>
                    @endif



                    @if ($usr->can('setting.view'))
                        <li> <a href="{{ route('admin.setting') }}"><span> <i class="fas fa-cog"></i> </span><span
                                    class="hide-menu">Generale Settings</span></a></li>
                    @endif




                </ul>
            </nav>
        </div>
    </aside>
    <div class="page-wrapper">
