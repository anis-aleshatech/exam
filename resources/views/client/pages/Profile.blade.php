@extends('client.layouts.app')
@section('title', 'My Profile')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="display-block text-center">

                        <a class="btn btn-primary reset-password m-2" title="Profile Update?"
                        href="{{ route('client.profileEdit', auth()->user()->id) }}"><i class="icon-line-edit "></i>Profile Update</a>
                    <img src="{{ auth()->user()->image ?? asset('/default-image.png') }}"
                        alt="{{ auth()->user()->name }}" width="150px" height="150px"
                        style="border-radius:50%; margin:20px  auto !important;">
                    <a class="btn btn-primary reset-password m-2" title="Reset Password?"
                        href="{{ route('client.passwordReset', auth()->user()->id) }}"><i class="icon-line-edit "></i>Pasword Update</a>
                </div>

                <table class="table table-borderless table-hover" style="padding:10px;">
                    <tr>
                        <td>name:</td>
                        <td>{{ auth()->user()->name }}</td>
                    </tr>
                    <tr>
                        <td>Email:</td>
                        <td>{{ auth()->user()->email }}</td>
                    </tr>
                    <tr>
                        <td>Mobile:</td>
                        <td>{{ auth()->user()->phone_number }}</td>
                    </tr>
                    <tr>
                        <td>Adress:</td>
                        <td>{{ auth()->user()->address }}</td>
                    </tr>
                    <tr>
                        <td>City:</td>
                        <td>{{ auth()->user()->city }}</td>
                    </tr>
                    <tr>
                        <td>District:</td>
                        <td>{{ auth()->user()->district }}</td>
                    </tr>
                    <tr>
                        <td>Zip code:</td>
                        <td>{{ auth()->user()->postal_code }}</td>
                    </tr>

                </table>
            </div>

        </div>


    </div>
@endsection

