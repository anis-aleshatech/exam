<header id="header" class="full-header header-size-md">
    <div id="header-wrap">
        <div class="container">
            <div class="header-row justify-content-lg-between">

                <!-- Logo
 ============================================= -->
                <div id="logo" class="mr-lg-4">
                    <a href="{{ route('client.home') }}" class="standard-logo"><img src="@if ($setting) {{ $setting->logo }} @endif"
                            alt="Logo"></a>
                    <a href="{{ route('client.home') }}" class="retina-logo"><img src="@if ($setting) {{ $setting->logo }} @endif"
                            alt="Logo"></a>


                </div><!-- #logo end -->

                <div class="header-misc">

                    <!-- Top Search
  ============================================= -->
                    <div >
                        @guest
                            <a href="#modal-register" data-lightbox="inline"><i
                                    class="icon-line2-user mr-1 position-relative" style="top: 1px;"></i><span
                                    class="d-none d-sm-inline-block font-primary font-weight-medium">Login</span></a>
                        @endguest

                        @auth
                            <a onClick="return confirm('Are you sure you want to Logout?')"
                                href="{{ route('client.logout') }}" class="d-none d-md-block"><i
                                    class="icon-line2-user mr-1 position-relative" style="top: 1px;"></i><span
                                    class="font-primary font-weight-medium ">Logout</span></a>

                            <a href="{{ route('client.profile') }}" class="text-success d-sm-block d-md-none"><i
                                    class="icon-line2-user mr-1 position-relative" style="top: 1px;"></i></a>
                        @endauth
                    </div><!-- #top-search end -->



                </div>



                <!-- Primary Navigation
 ============================================= -->
                @include('client.component.Menu')
                <!-- #primary-menu end -->



            </div>
        </div>
    </div>
</header><!-- #header end -->
