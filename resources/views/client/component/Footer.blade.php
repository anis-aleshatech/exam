<footer id="footer" class=" border-0" style="background-color: #cfcece; color:#000;">

    <!-- Copyrights
   ============================================= -->
    <div id="copyrights" class="bg-dark text-light">

        <div class="container clearfix">

            <div class="row justify-content-between align-items-center">
                <div class="col-md-12 text-center">
                    <p>Copyrights &copy; {{ date('Y') }} {{env('APPLICATION_NAME')}}. &nbsp;All Rights Reserved. </p>

                </div>
            </div>

        </div>

    </div><!-- #copyrights end -->

</footer>
