<nav class="primary-menu">
    <ul class="menu-container">
        @auth()
            <li class="menu-item custom-menu-my-account"><a class="menu-link" href="{{ route('client.profile') }}">
                    <div>My Account</div>
                </a>
            </li>
        @endauth


        <li class="menu-item"><a class="menu-link" href="{{ route('client.home') }}">
                <div>Home</div>
            </a>
        </li>




        @auth

            <li class="menu-item"><a class="menu-link" href="{{ route('client.profile') }}">
                    <div>Profile</div>
                </a>
            </li>
        @endauth

        @auth()
            <li class="menu-item d-sm-block d-md-none"><a onClick="return confirm('Are you sure you want to Logout?')"
                    class="menu-link" href="{{ route('client.logout') }}">
                    <div>
                        <i class="text-danger icon-line-power mr-1 position-relative" style="top: 1px;"></i>Logout
                    </div>
                </a>
            </li>
        @endauth
    </ul>

</nav>
